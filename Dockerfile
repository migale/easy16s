FROM rocker/verse:4.4.1
# RUN echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl', Ncpus = 4)" > /usr/local/lib/R/etc/Rprofile.site
ARG APP_NAME="shinyApp"
RUN mkdir -p /home/shiny/$APP_NAME
WORKDIR /home/shiny/$APP_NAME
ADD . /home/shiny/$APP_NAME
RUN Rscript -e 'renv::restore()'
RUN Rscript -e 'install.packages("remotes")'
RUN Rscript -e 'remotes::install_local(upgrade = "never")'
EXPOSE 3838
CMD  ["R", "-e", "options('shiny.port' = 3838, shiny.host = '0.0.0.0', shiny.maxRequestSize = 30 * 1024^2) ; easy16S::run_app(options = list(launch.browser = FALSE))"]
