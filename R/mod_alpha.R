#' alpha UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom plotly plotlyOutput
#' @importFrom shiny checkboxInput NS selectizeInput tagList textInput
#' @importFrom shinycssloaders withSpinner
#' @importFrom shinydashboard box
mod_alpha_ui <- function(id) {
  ns <- NS(id)
  tagList(
    plotlyOutput(ns("alpha_plot"), height = 700) |>
      withSpinner(),
    box(
      title = "Settings : ",
      width = NULL,
      status = "primary",
      col_4(
        selectizeInput(
          ns("alphaX"),
          label = "X : ",
          choices = NULL
        )
      ),
      col_4(
        selectizeInput(
          ns("alphaColor"),
          label = "Color : ",
          choices = NULL
        )
      ),
      col_4(
        selectizeInput(
          ns("alphaShape"),
          label = "Shape : ",
          choices = NULL
        )
      ),
      col_4(
        checkboxInput(
          ns("alphaBoxplot"),
          label = "Add boxplot (X should be non-empty)",
          value = FALSE
        )
      ),
      col_8(
        textInput(
          ns("alphaTitle"),
          label = "Title : ",
          value = "alpha diversity richness"
        )
      )
    )
  )
}

#' alpha Server Functions
#'
#' @noRd
#' @importFrom dplyr between
#' @importFrom ggplot2 element_blank element_text geom_boxplot scale_color_brewer scale_shape_manual theme theme_bw
#' @importFrom phyloseq get_variable plot_richness
#' @importFrom plotly renderPlotly
#' @importFrom rlang is_null
#' @importFrom shiny moduleServer need observeEvent reactive validate
mod_alpha_server <- function(id, r) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns

    output$alpha_plot <- renderPlotly({
      validate(
        need(slot_phyloseq(r$physeq), message = "Requires an abundance dataset")
      )

      p <- plot_richness(
        physeq = r$physeq,
        x = if (is_null(nullify(input$alphaX))) "samples" else nullify(input$alphaX),
        color = nullify(input$alphaColor),
        shape = nullify(input$alphaShape),
        title = nullify(input$alphaTitle),
        measures = c("Observed", "Chao1", "Shannon", "InvSimpson")
      ) +
        switch(between(nlevels(get_variable(r$physeq, nullify(input$alphaColor))), 1, 12),
          scale_color_brewer(palette = "Paired")
        ) +
        switch(not_null(nullify(input$alphaShape)),
          scale_shape_manual(values = seq(length(unique(get_variable(r$physeq, input$alphaShape)))))
        ) +
        switch(input$alphaBoxplot,
          geom_boxplot()
        ) +
        theme_bw() +
        theme(
          axis.text.x = element_text(angle = 90),
          axis.title.x = element_blank()
        )

      easy_ggplotly(
        p = p, tooltip = c("samples", "variable", "value", "x", "y", if (not_null(nullify(input$alphaColor))) "color", if (not_null(nullify(input$alphaShape))) "shape"), filename = "alpha", r = r,
        itemclick = TRUE, itemdoubleclick = TRUE
      )
    })

    observeEvent(
      r$physeq,
      {
        updateVariableToSelectize(session, "alphaX", reactive(r$physeq))
        updateVariableToSelectize(session, "alphaColor", reactive(r$physeq))
        updateVariableToSelectize(session, "alphaShape", reactive(r$physeq))
      }
    )
  })
}

## To be copied in the UI
# mod_alpha_ui("alpha_plot")

## To be copied in the server
# mod_alpha_server("alpha_plot", r = r)


#' alpha anova UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom DT DTOutput
#' @importFrom shiny em fluidRow NS p plotOutput selectizeInput tagList verbatimTextOutput
#' @importFrom shinycssloaders withSpinner
#' @importFrom shinydashboard box
mod_anova_ui <- function(id) {
  ns <- NS(id)
  tagList(
    box(
      title = "Settings : ",
      width = NULL,
      status = "primary",
      col_6(
        selectizeInput(
          ns("anovaMeasure"),
          label = "Richness measure : ",
          choices = c("Observed", "Chao1", "ACE", "Shannon", "Simpson", "InvSimpson", "Fisher"),
          selected = "Observed"
        ) |>
          embedInput("Alpha diversity",
            href = "https://joey711.github.io/phyloseq/plot_richness-examples.html"
          )
      ),
      col_6(
        selectizeInput(
          ns("anovaCovariate"),
          label = "Covariate : ",
          choices = NULL
        )
      )
    ),
    fluidRow(
      col_8(
        verbatimTextOutput(ns("anova")) |>
          withSpinner()
      ),
      col_4(plotOutput(ns("anovaResiduals")))
    ),
    box(
      title = "Table of pairwise comparisons",
      width = NULL,
      status = "primary",
      collapsible = TRUE,
      collapsed = TRUE,
      DTOutput(ns("anovaHSD")),
      footer = p("Here we compute", em("Tukey's Honest Significant Differences."), "For each pairwise comparison, the difference in observed means, the lower and upper endpoints of the confidence interval, and the p-value adjusted for multiple comparisons are provided."),
    )
  )
}

#' alpha anova Server Functions
#'
#' @noRd
#' @importFrom DT renderDT
#' @importFrom broom augment tidy
#' @importFrom dplyr across arrange inner_join join_by mutate select where
#' @importFrom ggplot2 aes geom_point ggplot labs stat_smooth theme
#' @importFrom phyloseq estimate_richness get_variable sample_data
#' @importFrom shiny moduleServer need observeEvent reactive renderPlot renderPrint validate
#' @importFrom stats anova aov as.formula TukeyHSD
#' @importFrom tibble as_tibble
mod_anova_server <- function(id, r) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns

    model <- reactive({
      validate(
        need(slot_phyloseq(r$physeq), message = "Requires an abundance dataset"),
        need(NA %not_in% phyloseq::get_variable(r$physeq, input$anovaCovariate), message = "Some NA values for this covariate. Please filter these samples.")
      )

      formula <- as.formula(paste(input$anovaMeasure, input$anovaCovariate, sep = "~"))

      anova_data <- inner_join(
        as_tibble(estimate_richness(r$physeq, measures = input$anovaMeasure), rownames = "Sample"),
        as_tibble(sample_data(r$physeq), rownames = "Sample"),
        by = join_by("Sample")
      )

      do.call("lm", list(formula = formula, data = as.name("anova_data")))
    })

    output$anova <- renderPrint({
      validate(
        need(slot_phyloseq(r$physeq), message = "Requires an abundance dataset"),
        need(NA %not_in% phyloseq::get_variable(r$physeq, input$anovaCovariate), message = "Some NA values for this covariate. Please filter these samples."),
        need(model(), "")
      )

      print(model())
      anova(model())
    })

    output$anovaResiduals <- renderPlot({
      validate(
        need(slot_phyloseq(r$physeq), message = "Requires an abundance dataset"),
        need(NA %not_in% phyloseq::get_variable(r$physeq, input$anovaCovariate), message = "")
      )

      model() |>
        augment() |>
        ggplot(aes(x = .data$.fitted, y = .data$.resid)) +
        geom_point(aes(col = .data[[input$anovaCovariate]])) +
        stat_smooth(method = "loess", formula = y ~ x, se = FALSE) +
        labs(title = "Residuals vs. Fitted", x = "Fitted values", y = "Residuals") +
        theme(legend.position = "bottom")
    })

    output$anovaHSD <- renderDT(
      {
        validate(
          need(slot_phyloseq(r$physeq), message = "Requires an abundance dataset"),
          need(model(), ""),
          need(
            class(get_variable(r$physeq, input$anovaCovariate)) %in% c("factor", "character"),
            "Selected covariate must be a factor or a character"
          )
        )

        model() |>
          aov() |>
          TukeyHSD() |>
          tidy() |>
          select("contrast", "estimate", "conf.low", "conf.high", "adj.p.value") |>
          arrange("adj.p.value") |>
          mutate(across(where(is.numeric), round, digits = 2))
      },
      server = FALSE,
      rownames = FALSE,
      filter = "top",
      options = list(dom = "t", paging = FALSE, scrollY = 300)
    )

    observeEvent(
      r$physeq,
      {
        updateVariableToSelectize(session, "anovaCovariate", allowNULL = FALSE, reactive(r$physeq))
      }
    )
  })
}

## To be copied in the UI
# mod_anova_ui("alpha_anova")

## To be copied in the server
# mod_anova_server("alpha_anova", r = r)
