#' table UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom DT DTOutput
#' @importFrom shiny NS radioButtons tagList
#' @importFrom shinycssloaders withSpinner
mod_table_ui <- function(id) {
  ns <- NS(id)
  tagList(
    if (id == "glom_table") {
      radioButtons(
        ns("rank"),
        label = "Taxonomic rank to agglomerate over : ",
        choices = "", selected = NULL, inline = TRUE
      )
    },
    DTOutput(ns("table")) |>
      withSpinner()
  )
}

#' table Server Functions
#'
#' @noRd
#' @importFrom DT renderDT
#' @importFrom dplyr relocate right_join select
#' @importFrom glue glue
#' @importFrom phyloseq otu_table rank_names tax_table
#' @importFrom phyloseq.extended fast_tax_glom
#' @importFrom shiny moduleServer need observeEvent reactive validate
#' @importFrom tibble rownames_to_column
mod_table_server <- function(id, r, slot = "otu_table", rownames = TRUE, fct = NULL) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns

    observeEvent(
      r$physeq,
      updateRanksToRadio(session = session, "rank", reactive(r$physeq), selected = 1)
    )

    output$table <- renderDT(
      {
        validate(
          need(slot_phyloseq(r$physeq, slot), message = glue("Requires {slot}"))
        )

        if (is.function(fct)) {
          fct(r$physeq)
        } else if (id == "glom_table") {
          phy_glom <- fast_tax_glom(r$physeq, taxrank = input$rank)
          otu <- otu_table(phy_glom) |>
            as.data.frame() |>
            rownames_to_column("OTU")

          tax <- tax_table(phy_glom) |>
            as.data.frame() |>
            rownames_to_column("OTU") |>
            relocate(rev(rank_names(phy_glom)))

          right_join(tax, otu, by = "OTU", keep = FALSE) |>
            select(-"OTU")
        }
      },
      server = FALSE,
      rownames = rownames,
      filter = "top",
      extensions = c("Buttons", "FixedColumns", "Scroller"),
      options = list(
        dom = "fBlti",
        buttons =
          list(
            extend = "collection",
            buttons = c("csv", "copy", "excel"),
            text = "Download"
          ),
        fixedColumns = list(dom = "t", leftColumns = 1, rightColumns = 0),
        scroller = TRUE,
        scrollX = TRUE,
        scrollY = 600
      ),
      width = "auto",
      height = "auto"
    )
  })
}

## To be copied in the UI
# mod_table_ui("table")

## To be copied in the server
# mod_table_server("table", r, slot, fct)
